from flask import Flask, render_template, url_for, abort
import os

app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<path:name>")
def index(name):
	source_path_bool = os.path.isfile('./templates/' + name)
	if ("//" in name) or (".." in name) or ("~" in name):
		abort(403)
	elif source_path_bool:
		return render_template(name)
	elif source_path_bool == False:
		abort(404)

@app.errorhandler(403)
def page_forbidden(error):
    return render_template('403.html'), 403

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404

if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=5000)
